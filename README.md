# GitLab SAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/sast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/sast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)

GitLab tool for running Static Application Security Testing (SAST) on provided
source code.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/code \
      --volume /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/gitlab-org/security-products/sast:${VERSION:-latest} /app/bin/run /code
    ```

    `VERSION` can be replaced with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-process) for more details.

1. The results will be displayed and also stored in `gl-sast-report.json`

**Why mounting the Docker socket?**

Some tools require to be able to launch Docker containers to scan your application. You can skip this but you won't benefit from all scanners.

### Environment variables

SAST can be configured with a number of environment variables, here is a list:

| Name                           | Function                                                                                                                                                                                   |
|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SAST_BRAKEMAN_LEVEL            | Only report Brakeman warnings above this confidence level. Integer, 1=Low 3=High.                                                                                                          |
| SAST_GOSEC_LEVEL               | Only report gosec warnings above this confidence level. Integer, 1=Low 3=High.                                                                                                             |
| SAST_FLAWFINDER_LEVEL          | Only report Flawfinder warnings above the risk level. Integer, 0=No risk, 5=High risk.                                                                                                     |
| SAST_ANALYZER_IMAGES           | Allow to specify custom Analyzer images (coma separated list). Read more about [customizing analyzers](./docs/analyzers.md#custom-analyzers).                                              |
| SAST_ANALYZER_IMAGE_PREFIX     | Allow to use a custom Docker registry that provides the official analyzers images under a different prefix. Read more about [customizing analyzers](./docs/analyzers.md#custom-analyzers). |
| SAST_DEFAULT_ANALYZERS_ENABLED | Allow to disable official Analyzer images. Read more about [customizing analyzers](./docs/analyzers.md#custom-analyzers).                                                                  |
| SAST_DOCKER_CLIENT_NEGOTIATION_TIMEOUT | Time limit for Docker client negotation          |
| SAST_PULL_ANALYZER_IMAGE_TIMEOUT       | Time limit when pulling the image of an analyzer |
| SAST_RUN_ANALYZER_TIMEOUT              | Time limit when running an analyzer              |

Timeouts are parsed using Go's [`ParseDuration`](https://golang.org/pkg/time/#ParseDuration).
Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
Examples: "300ms", "1.5h" or "2h45m".

## Development

### Running application

All you need to run SAST locally is a Go compiler:

```sh
go build
```

You can then run SAST on target directory `/tmp/code`:

```sh
CI_PROJECT_DIR=/tmp/code ./sast
```

You can test SAST against a particular set of analyzers
by setting `SAST_ANALYZER_IMAGES` and disabling the default images.
Also, you can instruct SAST not to pull the images if they're already available locally.
Here's how to run SAST using the Docker images `find-sec-bugs` and `flawfinder`:

```sh
CI_PROJECT_DIR=/tmp/code SAST_ANALYZER_IMAGES=find-sec-bugs,flawfinder ./sast --pull=false --default-images=false
```

To run the integration tests, simply run the Shell script `test.sh`
from the project directory:

```sh
./test.sh
```

## Supported languages, package managers and frameworks

The following table shows which languages, package managers and frameworks are supported and which tools are used.

| Language (package managers) / framework                                     | Scan tool                                                                              |
|-----------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| C/C++                                                                       | [Flawfinder](https://www.dwheeler.com/flawfinder/)                                     |
| Python ([pip](https://pip.pypa.io/en/stable/))                              | [bandit](https://github.com/openstack/bandit)                                          |
| Ruby on Rails                                                               | [brakeman](https://brakemanscanner.org)                                                |
| Java ([Maven](https://maven.apache.org/) and [Gradle](https://gradle.org/)) | [find-sec-bugs](https://find-sec-bugs.github.io/)                                      |
| Scala ([sbt](https://www.scala-sbt.org/))                                   | [find-sec-bugs](https://find-sec-bugs.github.io/)                                      |
| Go (experimental)                                                           | [gosec](https://github.com/securego/gosec)                                             |
| PHP                                                                         | [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit) |
| .NET                                                                        | [Security Code Scan](https://security-code-scan.github.io)                             |
| Node.js                                                                     | [NodeJsScan](https://github.com/ajinabraham/NodeJsScan)                                |

Tools are integrated into SAST via the analyzers, read the [documentation](./docs/analyzers.md) to know more about them.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
