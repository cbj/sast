FROM scratch
ADD sast /app/bin/run
ENTRYPOINT []
CMD ["/app/bin/run"]
