package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/urfave/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/bandit/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/brakeman/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/search"
	_ "gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-gradle/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-sbt/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/flawfinder/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gosec/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/plugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/plugin"
)

const (
	flagPullEnabled          = "pull"
	flagDefaultImages        = "default-images"
	flagAnalyzerImages       = "image"
	flagAnalyzersImagePrefix = "image-prefix"

	flagNegotiateTimeout = "negotiate-timeout"
	flagPullTimeout      = "pull-timeout"
	flagRunTimeout       = "run-timeout"

	envVarProjectDir = "CI_PROJECT_DIR"

	analyzersDefaultImagePrefix = "registry.gitlab.com/gitlab-org/security-products/analyzers"
	analyzersTag                = "11-1-stable"
)

func main() {
	flags := []cli.Flag{
		cli.BoolTFlag{
			Name:   flagPullEnabled,
			EnvVar: "SAST_PULL_ANALYZER_IMAGES,PULL_IMAGES",
			Usage:  "Pull Docker images",
		},
		cli.BoolTFlag{
			Name:   flagDefaultImages,
			EnvVar: "SAST_DEFAULT_ANALYZERS_ENABLED,DEFAULT_IMAGES",
			Usage:  "Enable default Docker images",
		},
		cli.StringSliceFlag{
			Name:   flagAnalyzerImages,
			EnvVar: "SAST_ANALYZER_IMAGES,ANALYZER_IMAGES",
			Usage:  "Docker images of analyzers",
		},
		cli.StringFlag{
			Name:   flagAnalyzersImagePrefix,
			EnvVar: "SAST_ANALYZER_IMAGE_PREFIX",
			Usage:  "Prefix of the docker images for analyzers",
			Value:  analyzersDefaultImagePrefix,
		},
		cli.StringFlag{
			Name:   flagNegotiateTimeout,
			EnvVar: "SAST_DOCKER_CLIENT_NEGOTIATION_TIMEOUT",
			Usage:  "Time limit for Docker client negotation",
			Value:  "2m",
		},
		cli.StringFlag{
			Name:   flagPullTimeout,
			EnvVar: "SAST_PULL_ANALYZER_IMAGE_TIMEOUT",
			Usage:  "Time limit when pulling the image of an analyzer",
			Value:  "5m",
		},
		cli.StringFlag{
			Name:   flagRunTimeout,
			EnvVar: "SAST_RUN_ANALYZER_TIMEOUT",
			Usage:  "Time limit when running an analyzer",
			Value:  "20m",
		},
	}
	flags = append(flags, search.NewFlags()...)

	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Perform SAST on given directory (using copy) or on $CI_PROJECT_DIR (mount binding)."
	app.ArgsUsage = "[project-dir]"
	app.Author = "GitLab"
	app.Flags = flags
	app.Action = func(c *cli.Context) error {
		// Check arguments
		if len(c.Args()) > 2 {
			cli.ShowSubcommandHelp(c)
			return errors.New("Invalid arguments")

		}

		// Register default images based on plugins
		images := []string{}
		if c.BoolT(flagDefaultImages) {
			for _, pname := range plugin.Names() {
				url := fmt.Sprintf("%s/%s:%s", c.String(flagAnalyzersImagePrefix), pname, analyzersTag)
				images = append(images, url)
			}
		}

		// Add extra images
		for _, image := range c.StringSlice(flagAnalyzerImages) {
			images = append(images, image)
		}

		// SAST configuration
		cfg := Config{
			SearchOpts:  search.NewOptions(c),
			PullEnabled: c.BoolT(flagPullEnabled),
			Images:      images,
		}

		// timeouts
		if d, err := time.ParseDuration(c.String(flagNegotiateTimeout)); err == nil {
			cfg.NegotiateTimeout = d
		} else {
			return err
		}

		if d, err := time.ParseDuration(c.String(flagPullTimeout)); err == nil {
			cfg.PullTimeout = d
		} else {
			return err
		}

		if d, err := time.ParseDuration(c.String(flagRunTimeout)); err == nil {
			cfg.RunTimeout = d
		} else {
			return err
		}

		// Target directory
		var projectDir string
		if c.Args().Present() {
			// Backward compatibility mode.
			log.Println("Copy project directory to containers")
			projectDir = c.Args().First()
			cfg.CopyProjectDir = true
		} else {
			// Mount $CI_PROJECT_DIR in containers.
			log.Println("Mount project directory in containers")
			projectDir = os.Getenv(envVarProjectDir)
			if projectDir == "" {
				log.Fatal(envVarProjectDir + " not set")
			}
		}

		return Analyze(projectDir, cfg)
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
