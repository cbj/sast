# GitLab SAST changelog

GitLab SAST follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/sast/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 11-2-stable
- Exit with code 4 when the directory given to scan is empty.

## 11-1-stable
- Add [NodeJsScan](https://github.com/ajinabraham/NodeJsScan) analyzer to scan NodeJS applications.
- Add `SAST_ANALYZER_IMAGE_PREFIX` env variable to allow usage of custom Docker registry.
- Rename `DEFAULT_IMAGES` env variable into `SAST_DEFAULT_ANALYZERS_ENABLED`.
- Rename `PULL_IMAGES` env variable into `SAST_PULL_ANALYZER_IMAGES`.
- Rename `ANALYZER_IMAGES` env variable into `SAST_ANALYZER_IMAGES`.
- Add environment variables to set the timeouts

## 11-0-stable
- Set `SAST_BRAKEMAN_LEVEL` default value to `1` (Low) for consistency with other tools
- Add [find-sec-bugs-sbt](https://find-sec-bugs.github.io/) analyzer to scan Scala sbt applications.
- Add [Security Code Scan](https://security-code-scan.github.io) analyzer to scan C# .NET applications
- Enrich report with more data

## 10-8-stable
- Add `SAST_GO_AST_SCANNER_LEVEL` env variable to filter out warnings from Go AST Scanner below given confidence level.
- Rename `SAST_CONFIDENCE_LEVEL` env variable to `SAST_BRAKEMAN_LEVEL` (keep old one for compatibility)
- Fix issue matching by adding fingerprints to issues.
- Propagate environment variables to analyzers.
- Detect languages and frameworks in sub directories (with max depth=2)
- Ignore some directories
- Add [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit) analyzer to scan PHP applications source code.
- Add [find-sec-bugs-graddle](https://find-sec-bugs.github.io/) analyzer to scan Java Graddle applications source code.

## 10-7-stable
- Add [Flawfinder](https://www.dwheeler.com/flawfinder/) analyzer to scan C/C++ applications source code.
- Add [Go AST Scanner](https://github.com/GoASTScanner/gas) analyzer to scan Go applications source code.
- Display solutions to vulnerabilities if available.
- Wrap text in report output.
- **Breaking Change:** Extract Dependency Scanning from SAST into a
  [dedicated project](https://gitlab.com/gitlab-org/security-products/dependency-scanning). An update of your
  `.gitlab-ci.yml` is necessary to keep analyzing your dependencies, please
  check the [documentation](https://docs.gitlab.com/ee/ci/examples/dependency_scanning.html).

## 10-6-stable
- Display a report of found vulnerabilities at the end.
- Add [find-sec-bugs](https://find-sec-bugs.github.io/) analyzer to scan Maven applications source code.

## 10-5-stable
- Change versioning and release process
- Add Gemnasium analyzer to check dependencies for various languages and package managers:
  * **Ruby**: rubygems.
  * **Javascript**: npm and yarn.
  * **PHP**: composer (with the _packagist_ registry).
  * **Python**: pip (with the _pypi_ registry). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
  * **Java**: maven (with the _central_ repository). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
- Add support for projects with multiple languages (execute all matching analyzers).
- Allow to disable analyzers that upload data to GitLab central server using `SAST_DISABLE_REMOTE_CHECKS` env variable.
- Fix priority for RetireJS issues.
- Fix generated file path for RetireJS issues.
- Dedupe issues comming from different tools and having the same CVE
- Rename `CONFIDENCE_LEVEL` env variable to `SAST_CONFIDENCE_LEVEL` (shows a deprecation warning if old name is still used).
- Exit with code 1 and warns user when Docker-in-Docker is needed but missing.

**Warning:** an update of the `sast` job config in `.gitlab-ci.yml` is necessary to benefit from the latest features, please check the [documentation](https://docs.gitlab.com/ee/ci/examples/sast.html).

## v0.3.0
- Fix CONFIDENCE_LEVEL env variable ignoring string value

## v0.3.0
- Allow pass CONFIDENCE_LEVEL env variable (works only with brakeman for now).

## v0.2.0
- Added Bandit, a python scanner.
- Sort issues in a report by priority.
