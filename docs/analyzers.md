# SAST Analyzers

## Overview

The SAST feature relies on underlying third party tools that are wrapped into what we call `Analyzers`.
An analyzer is a [dedicated project](https://gitlab.com/gitlab-org/security-products/analyzers) that wraps a particular tool to:
- expose its detection logic
- handle its execution
- convert its output to the common format

This is achieved by implementing the [common API](https://gitlab.com/gitlab-org/security-products/analyzers/common).

SAST currently supports the following official analyzers:

- [Bandit](https://gitlab.com/gitlab-org/security-products/analyzers/bandit)
- [Brakeman](https://gitlab.com/gitlab-org/security-products/analyzers/brakeman)
- [Find Sec Bugs (maven)](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs)
- [Find Sec Bugs (gradle)](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-gradle)
- [Find Sec Bugs (sbt)](https://gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-sbt)
- [Flawfinder](https://gitlab.com/gitlab-org/security-products/analyzers/flawfinder)
- [Gosec](https://gitlab.com/gitlab-org/security-products/analyzers/gosec)
- [NodeJsScan](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan)
- [PHP CS security-audit](https://gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit)
- [Security Code Scan (.NET)](https://gitlab.com/gitlab-org/security-products/analyzers/security-code-scan)

The Analyzers are published as Docker images that SAST will use to launch dedicated containers for each analysis.

## Custom Analyzers

The officially supported analyzers are provided via the GitLab Docker registry but you can customize SAST to your needs.

### Custom Docker registry

You can use the `SAST_ANALYZER_IMAGE_PREFIX` [environment variable](../README.md#environment-variables) to use a custom Docker registry that provides the official analyzer images under a different prefix. For instance, providing

    SAST_ANALYZER_IMAGE_PREFIX=my-docker-registry/gl-images

will tell SAST to pull the `my-docker-registry/gl-images/bandit` image instead of `registry.gitlab.com/gitlab-org/security-products/analyzers/bandit`.

Please note that this configuration requires that your custom registry provides images for all the official analyzers.

### Custom Analyzer Images

Another option to provide custom analyzer images is to use the `SAST_ANALYZER_IMAGES` [environment variable](../README.md#environment-variables) to specify Analyzers to be run by SAST. You can provide multiple Analyzers by separating the images names with a coma.

Please note that this configuration doesn't allow to benefit from the integrated detection step. SAST will always launch a container for each Analyzer images provided via this variable.

### Disable Official Analyzers

You can set the `SAST_DEFAULT_ANALYZERS_ENABLED` [environment variable](../README.md#environment-variables) to `false` to prevent SAST from fetching and running the official Analyzers.


## Analyzers Data


| Property \ Tool                       |       Bandit       |      Brakeman      |    Find Sec Bugs   |     Flawfinder     |   Go AST Scanner   |     NodeJsScan     | Php CS Security Audit | Security code Scan (.NET) |
|---------------------------------------|:------------------:|:------------------:|:------------------:|:------------------:|:------------------:|:------------------:|:---------------------:|:-------------------------:|
| severity                              | :white_check_mark: |         :x:        | :white_check_mark: |         :x:        | :white_check_mark: |         :x:        |   :white_check_mark:  |            :x:            |
| title                                 | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |   :white_check_mark:  |     :white_check_mark:    |
| description                           |         :x:        |         :x:        | :white_check_mark: |         :x:        |         :x:        | :white_check_mark: |          :x:          |            :x:            |
| file                                  | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |   :white_check_mark:  |     :white_check_mark:    |
| start line                            | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |   :white_check_mark:  |     :white_check_mark:    |
| end line                              | :white_check_mark: |         :x:        | :white_check_mark: |         :x:        |         :x:        |         :x:        |          :x:          |            :x:            |
| external id (e.g. CVE)                |         :x:        |      :warning:     |      :warning:     | :white_check_mark: |         :x:        |         :x:        |          :x:          |            :x:            |
| urls                                  |         :x:        | :white_check_mark: |      :warning:     |         :x:        |         :x:        |         :x:        |          :x:          |            :x:            |
| internal doc/explanation              |      :warning:     | :white_check_mark: | :white_check_mark: |         :x:        |         :x:        |         :x:        |          :x:          |            :x:            |
| solution                              |         :x:        |         :x:        |      :warning:     | :white_check_mark: |         :x:        |         :x:        |          :x:          |            :x:            |
| confidence                            | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |         :x:        |          :x:          |            :x:            |
| affected item (e.g. class or package) |         :x:        | :white_check_mark: | :white_check_mark: | :white_check_mark: |         :x:        |         :x:        |          :x:          |            :x:            |
| source code extract                   | :white_check_mark: | :white_check_mark: |         :x:        | :white_check_mark: | :white_check_mark: |         :x:        |          :x:          |            :x:            |
| internal id                           | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: | :white_check_mark: |         :x:        |   :white_check_mark:  |     :white_check_mark:    |

- :white_check_mark: => we have that data
- :warning: => we have that data but it's partially reliable, or we need to extract it from unstructured content
- :x: => we don't have that data or it would need to develop specific or inefficient/unreliable logic to obtain it.

The values provided by these tools are heterogeneous so they are sometimes normalized into common values (e.g. `severity`, `confidence`, etc).
This mapping usually happens in the analyzer's `convert` command.
