package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/docker/docker/client"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/common/search"
	"gitlab.com/gitlab-org/security-products/analyzers/common/walk"
	"gitlab.com/gitlab-org/security-products/sast/table"
)

const imageFindSecBugsGradle = "find-sec-bugs-gradle"

// Config configures the overall SAST analysis.
type Config struct {
	PullEnabled      bool
	Images           []string
	CopyProjectDir   bool
	SearchOpts       *walk.Options
	NegotiateTimeout time.Duration
	PullTimeout      time.Duration
	RunTimeout       time.Duration
}

// Analyze analyzes the project using all the analyzers that are available.
func Analyze(projectDir string, cfg Config) error {
	// Project directory check, it must not be empty
	dir, err := os.Open(projectDir)
	if err != nil {
		return err
	}
	defer dir.Close()

	_, err = dir.Readdirnames(1)
	if err == io.EOF {
		return errEmptyProjectDirectory
	}

	// Docker client
	cli, err := client.NewEnvClient()
	if err != nil {
		return err
	}

	// Negotiate API version
	ctx, cancel := context.WithTimeout(context.Background(), cfg.NegotiateTimeout)
	defer cancel()
	ping, err := cli.Ping(ctx)
	switch err {
	case nil:
		// OK
	case context.DeadlineExceeded:
		return ErrNegotiateTimeout
	default:
		return err

	}
	cli.NegotiateAPIVersionPing(ping)

	// Iterate over available images
	var compatFound bool
	issues := []issue.Issue{}
	for _, image := range cfg.Images {

		// Plugin name
		pname := strings.SplitN(filepath.Base(image), ":", 2)[0]
		logPrefix := "[" + pname + "] "

		// Attempt to check compatibility using plugin
		if match := plugin.Get(pname); match != nil {
			log.Println(logPrefix + "Detect project using plugin")
			_, err := search.New(match, cfg.SearchOpts).Run(projectDir)
			if _, ok := err.(search.ErrNotFound); ok {
				log.Printf(logPrefix + "Project not compatible")
				continue
			}
			if err != nil {
				return err
			}
			log.Printf(logPrefix + "Project is compatible")
		} else {
			log.Printf(logPrefix + "No detection plugin named")
		}

		// New analyzer
		log.Printf(logPrefix + "Starting analyzer...")
		opts := AnalyzerOptions{Client: cli}
		if pname == imageFindSecBugsGradle {
			/* HACK: force uid and gid when copying to find-sec-bugs-gradle.
			This is needed because:
			- Default user for this imagae is gradle, uid 1000.
			- The gradle command must run with the gradle user.
			- The gradle command requires write access to the project directory.
			- There's no way to know the user uid by inspecting the container.
			- We run the default command of the image, so we can't run "whoami".
			*/
			opts.TarOptions = TarOptions{SetOwner: true, UID: 1000, GID: 1000}
		}
		if cfg.CopyProjectDir {
			opts.ImportStrategy = ImportCopy
		}
		analyzer := NewAnalyzer(image, projectDir, opts)

		// Pull Docker image
		pullCtx, cancel := context.WithTimeout(context.Background(), cfg.PullTimeout)
		defer cancel()

		if cfg.PullEnabled {
			switch err := analyzer.Pull(pullCtx); err {
			case nil:
				// OK
			case context.DeadlineExceeded:
				return ErrPullTimeout
			default:
				return err
			}
		}

		// Run analyzer and parse artifact
		runCtx, cancel := context.WithTimeout(context.Background(), cfg.RunTimeout)
		defer cancel()

		issues2 := []issue.Issue{}
		err := analyzer.Run(runCtx, func(r io.Reader) error {
			return json.NewDecoder(r).Decode(&issues2)
		})

		switch err {
		case nil:
			// one compatible analyzer has been found
			compatFound = true
		case context.DeadlineExceeded:
			return ErrRunTimeout
		case ErrNotFound:
			// ignore
		default:
			// fail even if other analyzers work fine
			return err
		}

		// Merge issues
		for _, issue := range issues2 {
			issues = append(issues, issue)
		}
	}

	// Fail if no compatible analyzer
	if !compatFound {
		return errNoCompatibleAnalyzer
	}

	// Sort issues by severity
	sort.Slice(issues, func(i, j int) bool {
		return issues[i].Severity > issues[j].Severity
	})

	// Write artifact in project directory
	outputPath := filepath.Join(projectDir, artifactName)
	out, err := os.OpenFile(outputPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer func() {
		if err := out.Close(); err != nil {
			log.Println(err)
		}
	}()
	enc := json.NewEncoder(out)
	enc.SetIndent("", "  ")
	if err := enc.Encode(issues); err != nil {
		return err
	}

	// Render table in plain text
	t := table.New([]int{10, 10, 60})
	t.AppendSeparator()
	t.AppendCells("Severity", "Tool", "Location")
	t.AppendSeparator()
	for _, issue := range issues {
		location := fmt.Sprintf("%s:%d", issue.Location.File, issue.Location.LineStart)
		t.AppendCells(issue.Severity.String(), issue.Tool, location)
		t.AppendText("")
		t.AppendText(issue.Message)
		t.AppendSeparator()
	}
	t.Render(os.Stdout)
	return nil
}
