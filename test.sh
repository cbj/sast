#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..3"

failed=0
step=1

got="test/fixtures/gl-sast-report.json"
expect="test/expect/gl-sast-report.json"

# Project found, artifact generated (bind mount)
desc="Generate expected artifact (bind mount, pull images)"
rm -f $got
PATH="/whatever" CI_PROJECT_DIR="$PWD/test/fixtures" ./sast

if test $? -eq 0 && diff $got $expect; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project found, artifact generated (copy dir, no pull)
desc="Generate expected artifact (copy dir, no pull)"
rm -f $got
PATH="/whatever" ./sast --pull=false "test/fixtures"

if test $? -eq 0 && diff $got $expect; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project not found
desc="Exit with exit status 3 when no compatible analyzer can be found"
CI_PROJECT_DIR="$PWD/test/fixtures/unknown" ./sast --pull=false

if [ $? -eq 3 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Project empty
desc="Exit with exit status 4 when the project directory is completely empty"
mkdir -p "$PWD/test/fixtures/empty"
CI_PROJECT_DIR="$PWD/test/fixtures/empty" ./sast --pull=false

if [ $? -eq 4 ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
